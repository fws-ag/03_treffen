package main

import (
	"fmt"

	"ag/03_treffen/02_beispiel/basicmath"
)

func main() {
	fmt.Println(basicmath.Add(1, 2))
	fmt.Println(basicmath.Subtract(1, 2))

}
